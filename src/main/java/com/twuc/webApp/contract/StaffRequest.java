package com.twuc.webApp.contract;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StaffRequest {

    private Long id;

    @NotNull
    @Size(max = 64)
    private String firstName;

    @NotNull
    @Size(max = 64)
    private String lastName;

    private MyZoneId zoneId;

    public StaffRequest() {}

    public StaffRequest(@NotNull @Max(64) String firstName, @NotNull @Max(64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public MyZoneId getZoneId() {
        return zoneId;
    }
}
