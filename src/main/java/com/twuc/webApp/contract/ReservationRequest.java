package com.twuc.webApp.contract;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReservationRequest {

    @NotNull
    @Max(128)
    private String username;

    @NotNull
    @Max(64)
    private String companyName;

    @NotNull private ZoneId zoneId;
    @NotNull private ZonedDateTime startTime;

    @NotNull
    @Min(1)
    @Max(3)
    private Duration duration;

    public ReservationRequest(
            @NotNull @Max(128) String username,
            @NotNull @Max(64) String companyName,
            @NotNull ZoneId zoneId,
            @NotNull ZonedDateTime startTime,
            @NotNull @Min(1) @Max(3) Duration duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }
}
