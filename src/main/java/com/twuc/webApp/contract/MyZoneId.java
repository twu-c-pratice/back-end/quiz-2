package com.twuc.webApp.contract;

import java.time.ZoneId;

public class MyZoneId {
    private ZoneId zoneId;

    public MyZoneId() {}

    public static MyZoneId of(String zoneIdString) {
        ZoneId zoneId = ZoneId.of(zoneIdString);
        return new MyZoneId(zoneId);
    }

    public MyZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    @Override
    public String toString() {
        return zoneId.toString();
    }
}
