package com.twuc.webApp.domain;

import javax.persistence.*;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 128)
    private String username;

    @Column(nullable = false, length = 64)
    private String companyName;

    @Column(nullable = false)
    private ZoneId zoneId;

    @Column(nullable = false)
    private ZonedDateTime startTime;

    @Column(nullable = false)
    private Duration duration;

    @ManyToOne
    @JoinColumn(name = "staff_id")
    private Staff staff;

    public Reservation() {}

    public Reservation(
            String username,
            String companyName,
            ZoneId zoneId,
            ZonedDateTime startTime,
            Duration duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
