package com.twuc.webApp.web;

import com.twuc.webApp.contract.MyZoneId;
import com.twuc.webApp.contract.ReservationRequest;
import com.twuc.webApp.contract.StaffRequest;
import com.twuc.webApp.domain.Reservation;
import com.twuc.webApp.domain.ReservationRepository;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class StaffController {
    @Autowired private StaffRepository staffRepo;
    @Autowired private ReservationRepository reservationRepo;

    @PostMapping("/api/staffs")
    public ResponseEntity createStaff(@RequestBody @Valid StaffRequest staffRequest) {
        Staff staff =
                staffRepo.save(new Staff(staffRequest.getFirstName(), staffRequest.getLastName()));
        staffRepo.flush();
        return ResponseEntity.status(201)
                .header("Location", String.format("/api/staffs/%d", staff.getId()))
                .build();
    }

    @GetMapping("/api/staffs/{id}")
    public ResponseEntity getStaffById(@PathVariable Long id) {
        Optional<Staff> staffOptional = staffRepo.findById(id);

        if (!staffOptional.isPresent()) {
            return ResponseEntity.status(404).build();
        }

        Staff staff = staffOptional.get();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(staff);
    }

    @GetMapping("/api/staffs")
    public ResponseEntity getAllStaffs() {
        List<Staff> staffs = staffRepo.findAll();

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(staffs);
    }

    @PutMapping("/api/staffs/{staffId}/timezone")
    public ResponseEntity updateStaffZoneId(
            @PathVariable Long staffId, @RequestBody MyZoneId zoneId) {
        Optional<Staff> staffOptional = staffRepo.findById(staffId);

        if (!staffOptional.isPresent()) {
            return ResponseEntity.status(404).build();
        }

        Staff staff = staffOptional.get();
        staff.setZoneId(zoneId.toString());
        staffRepo.save(staff);
        staffRepo.flush();

        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/timezones")
    public ResponseEntity getAllTimeZoneIds() {
        Set<String> zoneIds = ZoneId.getAvailableZoneIds();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(zoneIds);
    }

    @PostMapping("/api/staffs/{staffId}/reservations")
    public ResponseEntity createReservationWithStaffId(
            @PathVariable Long staffId, @RequestBody ReservationRequest request) {
        Staff staff = staffRepo.findById(staffId).get();
        Reservation reservation =
                new Reservation(
                        request.getUsername(),
                        request.getCompanyName(),
                        request.getZoneId(),
                        request.getStartTime(),
                        request.getDuration());
        reservation.setStaff(staff);

        reservationRepo.save(reservation);
        reservationRepo.flush();

        return ResponseEntity.status(201)
                .header("Location", String.format("/api/staffs/%d/reservations", staff.getId()))
                .build();
    }
}
