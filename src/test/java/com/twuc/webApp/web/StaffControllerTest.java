package com.twuc.webApp.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.MyZoneId;
import com.twuc.webApp.contract.ReservationRequest;
import com.twuc.webApp.contract.StaffRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {

    void createStaff() throws Exception {
        StaffRequest staffRequest = new StaffRequest("Rob", "Hall");

        mockMvc.perform(
                post("/api/staffs")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(serialize(staffRequest)));
    }

    void updateStaffZoneId(Long staffId, MyZoneId zoneId) throws Exception {
        mockMvc.perform(
                put("/api/staffs/" + staffId + "/timezone")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(serialize(zoneId)));
    }

    @Test
    void should_create_staff() throws Exception {
        StaffRequest staffRequest = new StaffRequest("Rob", "Hall");

        mockMvc.perform(
                        post("/api/staffs")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(serialize(staffRequest)))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_get_staff_by_id() throws Exception {
        createStaff();

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("Hall"));
    }

    @Test
    void should_get_404_if_staff_id_is_not_exist() throws Exception {
        createStaff();

        mockMvc.perform(get("/api/staffs/2")).andExpect(status().is(404));
    }

    @Test
    void should_get_all_staffs() throws Exception {
        createStaff();
        createStaff();
        final ObjectMapper mapper = super.getMapper();

        MockHttpServletResponse response =
                mockMvc.perform(get("/api/staffs")).andReturn().getResponse();

        String responseString = response.getContentAsString();

        List<StaffRequest> staffs =
                mapper.readValue(responseString, new TypeReference<List<StaffRequest>>() {});

        assertEquals("Rob", staffs.get(0).getFirstName());
        assertEquals("Rob", staffs.get(1).getFirstName());
    }

    @Test
    void should_get_empty_array_if_no_staff() throws Exception {
        MockHttpServletResponse response =
                mockMvc.perform(get("/api/staffs")).andReturn().getResponse();

        String except = "[]";
        assertEquals(except, response.getContentAsString());
    }

    @Test
    void should_update_staff_time_zone_information() throws Exception {
        createStaff();
        MyZoneId zoneId = MyZoneId.of("Asia/Chongqing");

        mockMvc.perform(
                        put("/api/staffs/1/timezone")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(zoneId)))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_400_if_zone_id_invalid_when_update_staff_time_zone() throws Exception {
        createStaff();
        createStaff();

        ZoneId zoneId = null;

        mockMvc.perform(
                        put("/api/staffs/1/timezone")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(zoneId)))
                .andExpect(status().is(400));

        mockMvc.perform(
                        put("/api/staffs/1/timezone")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"zoneId\":\"Chongqing\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void get_staff_by_id_with_zone_information() throws Exception {
        MyZoneId zoneId = MyZoneId.of("Asia/Chongqing");

        createStaff();
        createStaff();
        updateStaffZoneId(1L, zoneId);

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));

        mockMvc.perform(get("/api/staffs/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.zoneId").doesNotExist());
    }

    @Test
    void should_get_all_zone_id() throws Exception {
        MockHttpServletResponse response =
                mockMvc.perform(get("/api/timezones")).andReturn().getResponse();

        String responseString = response.getContentAsString();
        final ObjectMapper mapper = super.getMapper();
        List<String> zoneIds =
                mapper.readValue(responseString, new TypeReference<List<String>>() {});

        assertIterableEquals(ZoneId.getAvailableZoneIds(), zoneIds);
    }

    @Test
    void should_create_reservation_with_staff_id() throws Exception {

        createStaff();

        ZoneId zoneId = ZoneId.of("Asia/Aqtau");
        ZonedDateTime startTime = ZonedDateTime.now().plusDays(20);
        Duration duration = Duration.ofHours(3);

        ReservationRequest reservationRequest =
                new ReservationRequest("Li", "TW", zoneId, startTime, duration);

        mockMvc.perform(
                        post("/api/staffs/1/reservations")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(serialize(reservationRequest)))
                .andExpect(status().is(201))
                .andExpect(header().string("Location", "/api/staffs/1/reservations"));
    }
}
